chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {

    if (changeInfo.status == 'complete' && tab.active && checkAddr(tab.url)) {

        chrome.scripting.executeScript({
            target: { tabId: tab.id },
            function: () => {

                doc = document.documentElement.innerHTML;
                console.log('Sending request');
                chrome.runtime.sendMessage({ doc: doc }, (response) => {
                    console.log('response: ', response);
                });

            },
        });

    }
    
})

chrome.runtime.onMessage.addListener(

    function (request, sender, sendResponse) {
        console.log(request);
        sendResponse({'status':true,'response':'Source code received'});
    }

);

function checkAddr(addr){
    if(addr.startsWith('http')){
        return true;
    }
}